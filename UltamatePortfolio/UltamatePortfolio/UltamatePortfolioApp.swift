//
//  UltamatePortfolioApp.swift
//  UltamatePortfolio
//
//  Created by Marcus Kane on 9/25/23.
//

import SwiftUI

@main
struct UltamatePortfolioApp: App {
    @StateObject var dataController = DataController()

    var body: some Scene {
        WindowGroup {
            NavigationSplitView {
                SideBarView()
            } content: {
                ContentView()
            } detail: {
                DetailView()
            }
                .environment(\.managedObjectContext, dataController.container.viewContext)
                .environmentObject(dataController)
        }
    }
}
